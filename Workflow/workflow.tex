\documentclass{beamer}

\usepackage{import}

\subimport{../}{git.tex}

\title{Git Workflow}
\subtitle{Using Git with examples}
\author{Isaac van Bakel}
\institute{Imperial College London}
\date{May 2017}
\subject{Version Control}

\begin{document}

\begin{frame}
        \titlepage
\end{frame}

\begin{frame}
	\frametitle{Cloning a repository}

	The first step to working on a repository is getting a local copy. You do this using a \inline{git clone}, which will create a new local repository with a single remote - \inline{origin}, the URL you used to clone from.

	The next step is usually to add any additional remotes, for other copies of the repository you might be interested in. If you use Github, for example, you'll typically push new commits to a local fork and get the latest commits from an upstream fork - so you \inline{git remote add upstream <URL>}.
\end{frame}

\begin{frame}
	\frametitle{Setting it up}

	The next step is getting it to the stage where you want to start editing. Most of the time, this is on the tip of the master branch, so you don't need to do anything!

	But sometimes, you'll need to switch to a development branch - \inline{git checkout <branch-name>} - or update your repository with new commits - \inline{git fetch <remote>} followed by \inline{git rebase <remote>/<branch-name>}.

	In rare cases, you'll want to work on a commit directly - \inline{git checkout <commit-ref>}.
\end{frame}

\begin{frame}
	\frametitle{Making edits}

	You are now free to make edits! Make some changes in your favourite editor to one or more files in your working tree.

	If you accidentally work on the wrong version, you can \inline{git stash} any changes, switch to the correct one, and \inline{git pop} to get back anything you changed.
\end{frame}

\begin{frame}
	\frametitle{Before the commit}

	Before making a commit, it's useful to consider \textit{what} you actually want to put into it. As a rule of thumb, it's a good idea to make it so that no commit would leave the repository in a state that's unusable. For code, that means compiling and passing tests.

	Equally, your commits are used to group changes together, so it would be useful if they actually split up sets of changes to be reapplied or reverted later. The best commits are \textbf{atomic} - they are the smallest set of changes required to do one thing to the repository, and not more than that.

	The better you split up your commits, the neater your history will be, and the easier it will be to use and manipulate in the future. Remember, a VCS that doesn't let you go back in time isn't worth anything.
\end{frame}

\begin{frame}
	\frametitle{Staging changes}

	Once there are edits to the working tree, you can start staging them to prepare for a commit. Most of the time, you'll want to add every change you made to a file with \inline{git add <file>}, or just \inline{git add -u} to add all the tracked files.

	It might be useful to only add part of a file, however. You can do this with \inline{git add -e}, but be careful of several pitfalls with the patch format - read the \inline{git-add} manual for more information.
\end{frame}

\begin{frame}[fragile]
	\frametitle{Making a commit}

	Now you've staged some changes, it's time to make them into a commit. The standard way is just to do \inline{git commit}, which will open up the editor you've configured to let you write the commit message.

	A good commit message looks like this:

	\begin{lstlisting}
	Commit Title

	Commit Description
	Commit Description
	%\dots%
	\end{lstlisting}

	When people see your commits in the log, they'll be shown the title, so make it descriptive.
\end{frame}

\begin{frame}
	\frametitle{Pushing}

	The final step to using Git is making changes available on other repositories. This is done through a \inline{git push}, which updates remotes with your local commits.

	A common situation is that the someone else has already put new commits on the remote branch. You'll first have to update your local history to match the remote, plus the new changes you've made.

	Sometimes, you'll also have rewritten history - for example, you might choose to fix your last commit because you forgot to include a necessary file. \textbf{Check for new commits on the remote first} - and then, if there aren't any, do a \inline{git push -f} - this will rewrite history, so be careful.
\end{frame}

\begin{frame}
	\frametitle{Updating local history}

	There are two ways to update local history - the painful way, and the proper way.

	The painful way is to use \inline{git pull} - this does a \inline{git fetch} followed by a \inline{git merge}. Merge commits are complicated, because they're powerful. Normally, you don't want them.

	The proper way is to use \inline{git fetch} and then \inline{git rebase}. This moves your new commits to be on top of the remote ones - history stays neat, and you'll experience far fewer issues.
\end{frame}

\begin{frame}[fragile]
	\frametitle{Resolving conflicts}

	When git encounters a case where two commits edit the same part of a file, it will mark it as a conflict for you to resolve. These take the format of

	\begin{lstlisting}
	<<<<<< ref
	Some code...
	===========
	Some more code...
	>>>>>> another ref
	\end{lstlisting}

	If you encounter a conflict, you'll need to edit the file to make a new, fixed version,  and mark it resolved - normally using \inline{git add}.
\end{frame}

\end{document}
