TEX = latex

.PHONY = all
all: intro commands history faq workflow

%.pdf : %.tex
	# Keeps produced files in their own directories
	cd $(@D) && pdf$(TEX) $(<F)

intro: Intro/intro.pdf

commands: Commands/commands.pdf

history: History/history.pdf

faq: FAQ/faq.pdf

workflow: Workflow/workflow.pdf
